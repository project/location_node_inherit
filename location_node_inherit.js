(function ($) {
  Drupal.behaviors.locationNodeInherit = function (context) {
    $('.form-select.location-node-inherit').change(function () {
      if ($(this).val() != '0') {
        $(this).parent().parent().children(':not(#' + $(this).parent().attr('id') + ')').hide();
      }
      else {
        $(this).parent().parent().children().show();
      };
    });
  };
}(jQuery));